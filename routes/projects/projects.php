<?php

Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'Projects'], function() {

        Route::get('/projects', 'ProjectsController@index')
            ->middleware('permission:read-projects')->name('projects:view');

        Route::group(['prefix' => 'projects'], function() {
            Route::view('/{project}/edit', 'projects.edit')->middleware('permission:edit-projects');
            Route::view('/create', 'projects.edit')->middleware('permission:edit-projects');
        });

        // API
        Route::group(['prefix' => 'api/projects'], function() {
            Route::post('/filter', 'ProjectsController@filter')->middleware('permission:read-projects');
            Route::get('/getAll', 'ProjectsController@getAll')->middleware('permission:read-projects');
            Route::get('/getone/{project}', 'ProjectsController@getOne')->middleware('permission:edit-projects');
            Route::get('/getoptions/{projectid}', 'ProjectsController@getOptions')->middleware('permission:edit-projects');
            Route::post('/getcode', 'ProjectsController@getCode')->middleware('permission:edit-projects');

            Route::post('/update/{project}', 'ProjectsController@update')->middleware('permission:edit-projects');
            Route::post('/store', 'ProjectsController@store')->middleware('permission:edit-projects');
            Route::post('/delete/{project}', 'ProjectsController@delete')->middleware('permission:edit-projects');

        });
    });
});
