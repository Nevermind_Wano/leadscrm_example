<?php

Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'TransportsCompany'], function() {
        // API
        Route::group(['prefix' => 'api/tc/boxberry'], function() {
            Route::get('/city_list', 'Boxberry@getCityList')->middleware('permission:read-projects');
            Route::get('/points_list/{city_id}', 'Boxberry@getListPoints')->middleware('permission:read-projects');
            Route::get('/delivery_cost', 'Boxberry@getDeliveryCost')->middleware('permission:read-projects');
        });

        Route::group(['prefix' => 'api/tc/pochta'], function() {
            Route::get('/city_list', 'Pochta@getCityList')->middleware('permission:read-projects');
            Route::get('/points_list/{city_id}', 'Pochta@getListPoints')->middleware('permission:read-projects');
            Route::get('/delivery_cost', 'Pochta@getDeliveryCost')->middleware('permission:read-projects');
        });
    });
});
