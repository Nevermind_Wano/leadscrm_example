<?php

Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'Goods'], function() {

        Route::get('/goods', 'GoodsController@index')
            ->middleware('permission:read-goods')
            ->name('goods:view');

        Route::group(['prefix' => 'goods'], function() {
            Route::view('/{good}/edit', 'goods.edit')->middleware('permission:edit-goods');
            Route::view('/create', 'goods.edit')->middleware('permission:edit-goods');
        });

        // API
        Route::group(['prefix' => 'api/goods'], function() {
            Route::post('/filter', 'GoodsController@filter')->middleware('permission:read-goods');
            Route::get('/variants/{good_variants}', 'GoodsController@getVariants')->middleware('permission:read-goods');
            Route::get('/getone/{good}', 'GoodsController@getOne')->middleware('permission:edit-goods');
            Route::get('/getall', 'GoodsController@getAll')->middleware('permission:read-goods');
            Route::get('/getoptions/{goodid}', 'GoodsController@getOptions')->middleware('permission:edit-goods');
            Route::get('/getbylead/{goods_lead}', 'GoodsController@getGoodsBylead')->middleware('permission:read-goods');


            Route::post('/update/{good}', 'GoodsController@update')->middleware('permission:edit-goods');
            Route::post('/store', 'GoodsController@store')->middleware('permission:edit-goods');
            Route::post('/uploadImage', 'GoodsController@uploadImage')->middleware('permission:edit-goods');

        });
    });
});
