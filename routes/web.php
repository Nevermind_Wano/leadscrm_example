<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('/register', function() {
    return '';
});

Auth::routes();

Route::get('/register', function() {
    return '';
});

Route::get('/dashboard', 'DashboardController@index')->name('home');

Route::get('/log/{order_log}', 'LogsController@get');

Route::name('output.add')->get('/ladd/', 'Orders\LeadsController@create')->middleware('cors');

Route::post('/addComment/', 'LogsController@saveComment')->middleware('permission:read-projects');


require __DIR__ . '/profile/profile.php';
require __DIR__ . '/users/users.php';
require __DIR__ . '/roles/roles.php';
require __DIR__ . '/roles/permissions.php';
require __DIR__ . '/goods/goods.php';
require __DIR__ . '/projects/projects.php';
require __DIR__ . '/orders/leads.php';
require __DIR__ . '/orders/orders.php';
require __DIR__ . '/modules/modules.php';
require __DIR__ . '/tc_api.php';

