<?php

Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'Orders'], function() {

        Route::get('/orders', 'OrdersController@index')
            ->middleware('permission:read-projects');

        Route::group(['prefix' => 'orders'], function() {
            Route::get('/create', 'OrdersController@create')->middleware('permission:read-projects');
            Route::get('/create/{lead}', 'OrdersController@createByLead')->middleware('permission:read-projects');
            Route::view('/{id}/edit', 'orders.create')->middleware('permission:read-projects');

        });

        // API
        Route::group(['prefix' => 'api/orders'], function() {
            Route::post('/filter', 'OrdersController@filter')->middleware('permission:read-projects');
            Route::get('/getallstatus', 'OrdersController@getAllStatus')->middleware('permission:read-projects');
            Route::post('/changestatus', 'OrdersController@changeStatus')->middleware('permission:read-projects');
            Route::post('/store', 'OrdersController@store')->middleware('permission:read-projects');
            Route::post('/update/{id}', 'OrdersController@update')->middleware('permission:read-projects');
            Route::post('/delete/{order}', 'OrdersController@delete')->middleware('permission:read-projects');
            Route::post('/getListNewOrders', 'OrdersController@getListNewOrders')->middleware('permission:read-projects');
            Route::get('/getone/{order}', 'OrdersController@getOne')->middleware('permission:read-projects');
            Route::post('/topackage', 'OrdersController@toPackage')->middleware('permission:read-projects');

        });
    });
});
