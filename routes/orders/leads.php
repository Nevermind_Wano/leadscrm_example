<?php

Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'Orders'], function() {

        Route::get('/leads', 'LeadsController@index')
            ->middleware('permission:read-projects');

        Route::group(['prefix' => 'leads'], function() {
            Route::get('/{lead}', 'LeadsController@showOne')->middleware('permission:read-projects');
        });

        // API
        Route::group(['prefix' => 'api/leads'], function() {
            Route::post('/filter', 'LeadsController@filter')->middleware('permission:read-projects');
            Route::get('/getallstatus', 'LeadsController@getAllStatus')->middleware('permission:read-projects');
            Route::post('/changestatus', 'LeadsController@changeStatus')->middleware('permission:read-projects');
            Route::get('/{lead}', 'LeadsController@getOneData')->middleware('permission:read-projects');
            Route::get('/getdata/{lead_data}', 'LeadsController@getLeadData')->middleware('permission:read-projects');
            Route::post('/reject/{lead}', 'LeadsController@setReject')->middleware('permission:read-projects');
        });
    });
});
