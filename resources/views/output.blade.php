{!! form($form) !!}

<script>
    $('form.leads_form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: '{{route('output.add')}}',
            type: 'GET',
            data: $('form.leads_form').serialize() + '&source=' + window.location.host,
            contentType: 'multipart/form-data',
            processData: false,
            success: function (data) {
                let filename = 'success.html';
                window.location.href = window.location.href.split('#')[0] + filename;
            },
        })
    });
</script>

