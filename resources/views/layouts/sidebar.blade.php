<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/dashboard">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
            @can('read-users','read-roles')
            <li class="nav-title">Настройки</li>
            @endcan

            @can('read-users')
            <li class="nav-item">
                <a class="nav-link" href="/users">
                    <i class="nav-icon icon-people"></i> Пользователи
                </a>
            </li>
            @endcan
            @can('read-roles')
            <li class="nav-item">
                <a class="nav-link" href="/roles">
                    <i class="nav-icon icon-key"></i> Роли
                </a>
            </li>
            @endcan


            <li class="nav-title">Управление заказами</li>

            @can('read-projects')
                <li class="nav-item">
                    <a class="nav-link" href="/projects">
                        <i class="nav-icon icon-people"></i> Проекты
                    </a>
                </li>
            @endcan
            @can('read-goods')
                <li class="nav-item">
                    <a class="nav-link" href="/goods">
                        <i class="nav-icon icon-key"></i> Товары
                    </a>
                </li>
            @endcan
            @can('read-projects')
                <li class="nav-item">
                    <a class="nav-link" href="/leads">
                        <i class="nav-icon icon-key"></i> Лиды (<small>новые заявки</small>)
                    </a>
                </li>
            @endcan
            @can('read-projects')
                <li class="nav-item">
                    <a class="nav-link" href="/orders">
                        <i class="nav-icon icon-key"></i> Заказы
                    </a>
                </li>
            @endcan
        </ul>
    </nav>
    <sidebar></sidebar>
</div>
