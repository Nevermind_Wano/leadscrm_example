<?php

/**
 * ТИПЫ ЛОГОВ
 */

if ( ! defined( 'LOG_change_status' ) ){
    define('LOG_change_status', 0);
}

if ( ! defined( 'LOG_change_type' ) ){
    define('LOG_change_type', 1);
}

if ( ! defined( 'LOG_comment') ){
    define('LOG_comment', 2);
}

if ( ! defined( 'LOG_error' ) ){
    define('LOG_error', 3);
}

/**
 * СТАТУСЫ ЛИДОВ
 */

if ( ! defined( 'LEAD_STATUS_new' ) ){
    define('LEAD_STATUS_new', 0);
}

if ( ! defined( 'LEAD_STATUS_accepted' ) ){
    define('LEAD_STATUS_accepted', 1);
}

if ( ! defined( 'LEAD_STATUS_callback' ) ){
    define('LEAD_STATUS_callback', 2);
}

if ( ! defined( 'LEAD_STATUS_reject' ) ){
    define('LEAD_STATUS_reject', -1);
}

if ( ! defined( 'LEAD_STATUS_no_call_3_day' ) ){
    define('LEAD_STATUS_no_call_3_day', -2);
}

if ( ! defined( 'LEAD_STATUS_double' ) ){
    define('LEAD_STATUS_double', -3);
}

/**
 * СТАТУСЫ ЗАКАЗОВ
 */

if ( ! defined( 'ORDER_STATUS_new' ) ){
    define('ORDER_STATUS_new', 0);
}

if ( ! defined( 'ORDER_STATUS_to_package' ) ){
    define('ORDER_STATUS_to_package', 1);
}

if ( ! defined( 'ORDER_STATUS_to_transport' ) ){
    define('ORDER_STATUS_to_transport', 2);
}


///**
// * СТАТУСЫ ОТПРАВЛЕНИЙ
// */
//
//if ( ! defined( 'LEAD_DEPARTURE:dispatched' ) ){
//    define('LEAD_STATUS:dispatched', 0);
//}
//
//if ( ! defined( 'LEAD_DEPARTURE:transit' ) ){
//    define('LEAD_STATUS:transit', 1);
//}
//
//if ( ! defined( 'LEAD_DEPARTURE:arrived' ) ){
//    define('LEAD_STATUS:arrived', 2);
//}
//
//if ( ! defined( 'LEAD_DEPARTURE:delivered' ) ){
//    define('LEAD_STATUS:delivered', 3);
//}
//
//if ( ! defined( 'LEAD_DEPARTURE:error' ) ){
//    define('LEAD_STATUS:error', -1);
//}
//
//if ( ! defined( 'LEAD_DEPARTURE:return' ) ){
//    define('LEAD_STATUS:return', -2);
//}
//
///**
// * ТРАНСПОРТНЫЕ КОМПАНИИ
// */
//
//if ( ! defined( 'TRANSPORT:boxberry' ) ){
//    define('TRANSPORT:boxberry', 1);
//}
