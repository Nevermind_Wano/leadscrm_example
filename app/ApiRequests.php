<?php


namespace App;

use GuzzleHttp\Client;

abstract class ApiRequests
{
    protected
        $client,
        $baseurl,
        $proxy = [];

    public function __construct($baseUrl, array $proxy = [])
    {
//        $curl = $this->getProxyString($proxy);

        $args = [
            // Base URI is used with relative requests
            'base_uri' => $baseUrl,
//            $curl,
            // You can set any number of default request options.
            'timeout'  => 20.0,
        ];

//        if ($curl)
//            $args['curl'] = $curl;

        $this->client = new Client($args);
    }

    private function getProxyString(array $proxy): ?array
    {
//        return "https://{$proxy['username']}:{$proxy['pass']}@{$proxy['ip']}";
        if (!empty($proxy))
        {
            $ip = explode(':', $proxy['ip']);

            return
                [
                    CURLOPT_SSLVERSION   => CURL_SSLVERSION_TLSv1_2,
                    CURLOPT_PROXY        => $ip[0],
                    CURLOPT_PROXYPORT    => $ip[1],
                    CURLOPT_PROXYUSERPWD => "{$proxy['username']}:{$proxy['pass']}"
                ];

        }

        return null;
    }


    public function destroyClient() {
        $this->client = NULL;
    }

    public function get(string $url, array $queryParams): string {
        $response = $this->client->request('GET', $this->baseurl . $url, [
            'query' => $queryParams
        ]);

        if ($response->getStatusCode() === 200)
        {
//            $this->client->delete();
            return $response->getBody()->getContents();
        }

        return 'error';
    }

    public function post(string $url, array $queryParams, array $headers = []): string
    {
//        dd($queryParams);
        $response = $this->client->request('POST', $this->baseurl . $url, [
            'json' => $queryParams,
            'headers' => $headers
        ]);

        if ($response->getStatusCode() === 200)
        {
//            $this->client->delete();
            return $response->getBody()->getContents();
        }

        return 'error';
    }


    public function send(string $urlParams, array $data): bool {
        return true;
    }


}
