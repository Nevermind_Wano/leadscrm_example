<?php


namespace App\Helpers;

use App\Models\Fields;
use App\Models\FieldTypes;
use Kris\LaravelFormBuilder\Form;

class OutputForm extends Form
{
    public static $project_id = 0;

    public function buildForm()
    {
        if (self::$project_id !== 0)
        {
            $fields = Fields::where('project_id', self::$project_id)->get();
            foreach ($fields as $field)
            {
                $type = FieldTypes::where('id', $field->field_type)->first();
                $this->add($field->field_name, $type->name, [
                    'wrapper' => ['class' => $field->wrapper_class],
                    'attr' => [
                        'class' => $field->attr_class,
                    ],
                    'default_value' => null, // Fallback value if none provided by value property or model
                    'label' => $field->label,  // Field name used
                    'label_attr' => ['class' =>  $field->label_class],
                    'label_show' => true,
                    'errors' => ['class' => $field->errors_class],
                    'rules' => ($field->required_field) ? 'required' : '', // Validation rules
                    'error_messages' => ['error_messages'],   // Validation error messages
                ]);
            }
            $this->add('id', 'hidden',[
                'default_value' => self::$project_id
            ]);
            $this->add('submit', 'submit', ['label' => 'Отправить']);
        }
    }
}
