<?php


namespace App\Helpers;

use App\Models\OrdersLog;
use Illuminate\Support\Facades\Auth;


class Logs
{
    public static function set(int $lead_id, int $type, string $data)
    {
        $user_id = (Auth::id()) ?: 1;
        $log = new OrdersLog();
        $log->user_id = $user_id;
        $log->lead_id = $lead_id;
        $log->log_type_id = $type;
        $log->data = $data;
        $log->save();

    }

    public static function get()
    {

    }
}
