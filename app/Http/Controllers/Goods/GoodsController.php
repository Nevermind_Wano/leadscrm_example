<?php

namespace App\Http\Controllers\Goods;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Upload;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Goods;
use App\Models\Projects;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;

class GoodsController extends Controller
{
    public function index()
    {
        return view('goods.index');
    }

    public function filter(Request $request)
    {
        $query = Goods::query();

        $query->whereNull('is_variant');

        $query->variantsCount();

        if ($request->search)
        {
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $goods = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $goods;
    }

    public function getVariants(Collection $goods)
    {
        return $goods;
    }

    public function getOne(Collection $goods)
    {
        return json_encode([
            'name'         => $goods[0]->name,
            'goods'        => $goods,
            'projects'     => $goods[0]->projects()->get(),
            'general'      => $goods[0]->general()->get(),
            'concomitants' => $goods[0]->concomitants()->get()
        ]);
    }

    public function getGoodsBylead(Collection $goods)
    {
        $_concomitants = $goods[0]->concomitants()->withVariants()->get();
        foreach ($_concomitants as $good)
        {
            $good->customFields = $good->customFields()->get();
            $good->clientCount = 0;
            $good->clientPrice = 0;
        }
        $concomitants = [];
        foreach ($_concomitants as $good)
        {
            if ($good->is_variant == null)
            {
                $concomitants[$good->id] = [
                    'name'  => $good->name,
                    'id'    => $good->id,
                    'goods' => [$good]
                ];
                continue;
            }
            array_push($concomitants[$good->is_variant]['goods'], $good);
        }

        return json_encode([
            'orderGoods'   => $goods,
            'concomitants' => array_values($concomitants)
        ]);
    }

    public function getAll()
    {
        return json_encode(Goods::All());
    }

    public function getOptions($good_id = 0)
    {
        $allGoods = Goods::where('id', '<>', $good_id)
            ->where('is_variant', null)
            ->get();

        $allProjects = [];

        foreach (Projects::all() as $project)
        {
            if ($project->goods()->get()->isEmpty())
                array_push($allProjects, $project->goods()->get());
        }

        return json_encode([
            'allGoods'    => $allGoods,
            'allProjects' => $allProjects,
        ]);
    }

    public function update(Collection $goods, Request $request)
    {
        $good_id = 0;
        foreach ($goods as $good)
        {
            if ($good->is_variant)
                $good->delete();
            else
                $good_id = $good->id;
        }

        return $this->store($request, $good_id);
    }

    public function store(Request $request, int $good_id = 0)
    {
        $requestData = $request->all();
        $goodsArray = $requestData['goods'];

        $mainGoodId = null;

        foreach ($goodsArray as $key => $data)
        {
            if (empty($data['is_variant']) && isset($data['id']))
                $good = Goods::find($data['id']);
            else
                $good = new Goods();

            unset($data['id']);
            $customFields = $data['customFields'];
            unset($data['customFields']);
            $good->fill($data);
            $good->name = ($key === 0) ? $requestData['name'] : null;
            $good->is_variant = $mainGoodId;
            $good->save();

            if ($key === 0)
                $mainGoodId = $good->id;

            $good->attachCF($customFields);
            if ($key !== 0)
                continue;

            $good->attachProjects($requestData['projects']);
            $good->attachConcomitants($requestData['concomitants']);
            $good->attachGenerals($requestData['general']);
        }

    }

    public function uploadImage(Request $request)
    {
        $upload = new Upload();
        $image = $upload->upload($request->file, 'goods/' . str_random(15))->resize(600, 600)->getData();

        return json_encode([
            'image_url' => Storage::url($image['path'])
        ]);
    }
}
