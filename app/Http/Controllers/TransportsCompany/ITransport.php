<?php


namespace App\Http\Controllers\TransportsCompany;


use Illuminate\Http\Request;

interface ITransport
{
    public function getCityList();
    public function getDeliveryCost(Request $request);
}
