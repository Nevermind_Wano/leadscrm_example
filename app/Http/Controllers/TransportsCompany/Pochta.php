<?php


namespace App\Http\Controllers\TransportsCompany;


use App\ApiRequests;
use Illuminate\Http\Request;

class Pochta extends ApiRequests implements ITransport
{
    private $url = 'https://otpravka-api.pochta.ru',
            $token = env('POCHTA_TOKEN'),
            $key = env('POCHTA_KEY');

    public function __construct()
    {
        parent::__construct($this->url);
    }

    public function getCityList()
    {
        // TODO: Implement getCityList() method.
    }

    public function getDeliveryCost(Request $request)
    {
        $data = $request->all();
        $response = $this->post('/1.0/tariff', [
            'index-from' => 109440,
            'index-to' => $data['zip'],
            'mass'  => $data['weight'],
            'declared-value'  => $data['price'] * 100,
            'mail-category' => 'WITH_DECLARED_VALUE_AND_CASH_ON_DELIVERY',
            'mail-type' => 'POSTAL_PARCEL',
            "with-order-of-notice" => "false",
            "with-simple-notice" => "false"
        ],
        [
            'Content-Type' => "application/json;charset=UTF-8",
//            'Accept'       => "application/json;charset=UTF-8",
            'Authorization' => $this->token,
            'X-User-Authorization' => $this->key,

        ]);



        $response = json_decode($response);

          return [
            'delivery_period' => $response->{'delivery-time'}->{'max-days'},
            'price' => $response->{'total-rate'}/100 + $response->{'total-vat'}/100,
        ];

//        try {
//            $objectId = 2020; // Письмо с объявленной ценностью
//            // Минимальный набор параметров для расчета стоимости отправления
//            $params = [
//                'weight' => $data['weight'], // Вес в граммах
//                'sumoc' => 10000, // Сумма объявленной ценности в копейках
//                'from' => $data['zip'] // Почтовый индекс места отправления
//            ];
//
//            // Список ID дополнительных услуг
//            // 2 - Заказное уведомление о вручении
//            // 21 - СМС-уведомление о вручении
//            $services = [2,21];
//
//            $TariffCalculation = new \LapayGroup\RussianPost\TariffCalculation();
//            $calcInfo = $TariffCalculation->calculate($objectId, $params, $services);
//
//            dd($calcInfo);
//        }
//
//        catch (\LapayGroup\RussianPost\Exceptions\RussianPostTarrificatorException $e) {
//            // Обработка ошибок тарификатора
//            $errors = $e->getErrors(); // Массив вида [['msg' => 'текст ошибки', 'code' => код ошибки]]
//            return $errors;
//        }
//
//        catch (\LapayGroup\RussianPost\Exceptions\RussianPostException $e) {
//            return $e->getMessage();
//        }
//
//        catch (\Exception $e) {
//            return $e->getMessage();
//        }
    }
}
