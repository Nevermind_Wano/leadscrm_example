<?php


namespace App\Http\Controllers\TransportsCompany;
use App\ApiRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class Boxberry extends ApiRequests implements ITransport
{

    private $url = 'https://api.boxberry.ru',
            $token = env('BOXBERRY_TOKEN');

    public function __construct()
    {
        parent::__construct($this->url);
    }

    public function getCityList()
    {
        return Cache::remember('boxberry_cities', 720, function () {
            return $this->get('/json.php', [
                'token' => $this->token,
                'method' => 'ListCitiesFull'
            ]);
        });
    }

    public function getListPoints($cityId)
    {
        return Cache::remember('boxberry_points_' . $cityId, 720, function () use ($cityId) {
            return $this->get('/json.php', [
                'token' => $this->token,
                'CityCode' => $cityId,
                'method' => 'ListPoints'
            ]);
        });
    }

    public function getDeliveryCost(Request $request)
    {
        $data = $request->all();
        return $this->get('/json.php', [
            'token' => $this->token,
            'target' => $data['pointCode'],
            'weight' => $data['weight'],
            'zip'    => (isset ($data['zip'])) ? $data['zip'] : null,
            'method' => 'DeliveryCosts',
            'deliverysum' => $data['price'],
            'paysum' => $data['price']
        ]);

    }
}
