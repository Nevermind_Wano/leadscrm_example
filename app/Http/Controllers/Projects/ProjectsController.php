<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Models\Fields;
use App\Models\FieldTypes;
use App\Models\Goods;
use Illuminate\Http\Request;
use App\Models\Projects;
use Illuminate\Support\Facades\Schema;
use App\Helpers\OutputForm;
use Kris\LaravelFormBuilder\FormBuilder;

class ProjectsController extends Controller
{
    public function index()
    {
        return view('projects.index');
    }

    public function filter (Request $request)
    {
        $query = Projects::query();

        if($request->search) {
            $query->where('name', 'LIKE', '%'.$request->search.'%');
        }

        $projects = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $projects;
    }

    public function getAll()
    {
        $projects = Projects::all();

        foreach ($projects as $project)
        {
            $project->fields = $project->fields()->get();
            foreach ($project->fields as $field)
                $field->field_type = FieldTypes::find($field->field_type);
        }

        return $projects;

    }

    public function getOne(Projects $project)
    {
        $fields = $project->fields()->get();
        foreach ($fields as $field)
           $field->field_type = FieldTypes::find($field->field_type);

        return json_encode([
            'project' =>  $project,
            'goods' =>  $project->goods()->get(),
            'fields'   => $fields
        ]);
    }

    public function getOptions()
    {
        $allGoods = Goods::all();
        $fieldTypes = FieldTypes::all();

        $field =  new Fields();
        // get the column names for the table
        $columns = Schema::getColumnListing($field->getTable());
        // create array where column names are keys, and values are null
        $columns = array_fill_keys($columns, null);

        return json_encode([
            'allGoods' => $allGoods,
            'fieldTypes' => $fieldTypes,
            'emptyField' => $columns
        ]);
    }

    public function getCode(FormBuilder $formBuilder, Request $request)
    {
        $id = $request->get('project_id');

        OutputForm::$project_id = $id;
        $form = $formBuilder->create('App\Helpers\OutputForm', [
            'method' => 'GET',
            'class'  => 'leads_form',
            'url'    => '#',
        ]);

        $route = route('output.add');

        return view('output', compact('form', 'route'))->render();
    }

    public function update(Projects $project, Request $request)
    {
        $data = $request->all();
        $project->update($data['project']);

        $project->attachGoods($data['goods']);
        $project->attachFields($data['fields']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $project = new Projects();
        $project->fill($data['project']);
        $project->save();

        $project->attachGoods($data['goods']);
        $project->attachFields($data['fields']);
    }

    public function delete(Projects $project)
    {
        $project->delete();
        return 1;
    }


}
