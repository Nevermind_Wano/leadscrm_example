<?php

namespace App\Http\Controllers;

use App\Models\OrdersLog;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\User;
use App\Helpers\Logs;

class LogsController extends Controller
{
    public function get(Collection $logs)
    {
        foreach ($logs as $log){
            $log->alert_class = $this->getAlertClass($log);
            $log->user = User::find($log->user_id)->name;
        }


        return json_encode($logs);
    }

    public function saveComment(Request $request)
    {
        $data = (object)$request->all();

        Logs::set($data->id, LOG_comment, $data->data);
    }

    private function getAlertClass(OrdersLog $log)
    {
        switch($log->log_type_id) {
            case LOG_change_status:
                return 'alert-info';
                break;

            case LOG_change_type:
                return 'alert-primary';
                break;

            case LOG_comment:
                return 'alert-secondary';
                break;

            case LOG_error:
                return 'alert-danger';
                break;
        }
    }
}
