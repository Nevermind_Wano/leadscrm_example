<?php

namespace App\Http\Controllers\Orders;

use App\Models\LeadsStatuses;
use App\Models\OrdersLog;
use App\Models\OrdersStatuses;
use App\Models\Projects;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Leads;
use App\Models\LeadsData;
use App\Helpers\Logs;
use App\Models\Orders;
use App\Mail\LeadSend;
use Illuminate\Support\Facades\Mail;

class LeadsController extends Controller
{
    public function index()
    {
        return view('leads.index');
    }

    public function filter (Request $request)
    {
        $query = Leads::query();

        if (!$request->withOld)
            $query->where('status_id', '<>', LEAD_STATUS_accepted);
            $query->where('status_id', '=', $request->status_id['id']);

        if ($request->status_id['id'] === LEAD_STATUS_reject)
            $query->withTrashed();


        if($request->search) {
            $query->where('id', 'LIKE', '%'.$request->search.'%');
        }

        $leads = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        foreach ($leads as $lead)
        {
            $order = Orders::where('lead_id', $lead->id)->first();
            if ($order && $order->status_id !== ORDER_STATUS_new)
                $lead->status = OrdersStatuses::find($order->status_id)->name;
            else
                $lead->status = $lead->status()->first()->name;

            $lastComment = $this->getLastComment($lead->id);



            $lead->lastComment = ($lastComment) ? $lastComment->data : null;
            $lead->fields = LeadsData::getData($lead->id);

        }

        return $leads;
    }

    public function getLeadData(Collection $data)
    {
        return json_encode($data);
    }

    public function showOne(Leads $lead)
    {
        return view('leads.view');
    }

    public function getOneData(Leads $lead)
    {
        $lead->data = $lead->data()->get();

        return json_encode($lead);
    }

    public function setReject(Leads $lead, Request $request)
    {
        $data = $request->all();

        $lead->status_id = $data['rejectType'];
        $lead->save();

        if (intval($lead->status_id) === LEAD_STATUS_reject)
            $lead->delete();

        $statusText = LeadsStatuses::where('id', $lead->status_id)->first();
        Logs::set($lead->id, LOG_change_status, 'Изменён статус на "' . $statusText->name . '"');

        if (!empty($data['comment']))
            Logs::set($lead->id, LOG_comment, $data['comment']);
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $mailData = [];

        $id = $data['id'];
        unset($data['id']);

        $lead = new Leads();
        $lead->project_id = $id;
        $lead->source = $data['source'];
        unset($data['source']);
        $lead->save();

        list($double, $doubleID, $leadData) = $lead->saveData($data);

        Logs::set($lead->id, LOG_change_status, 'Новый лид id=' . $lead->id);



        if ($double) {
            $status_id = LEAD_STATUS_double;
            $lead->status_id = $status_id;
            $statusText = LeadsStatuses::where('id', $status_id)->first();
            Logs::set($lead->id, LOG_change_status, 'Изменён статус на "' . $statusText->name . '"');
            Logs::set($lead->id, LOG_comment, 'Является дублем лида id=' . $doubleID);
            $leadData['double'] = true;
            $leadData['doubleID'] = $doubleID;
        }

        $lead->save();

        $project = Projects::where('id', $id)->first();

//        Mail::to('tovarka@pimac.ru')->send(new LeadSend($project->name, $leadData));


        return $lead->id;
    }

    public function getAllStatus(Request $request)
    {
        return LeadsStatuses::all();
    }

    public function changeStatus(Request $request)
    {
        $data = (object)$request->all();

        $lead = Leads::find($data->lead_id);
        $lead->status_id = $data->status;
        $lead->save();

        if ($data->status === LEAD_STATUS_reject)
            $lead->delete();


        $statusText = LeadsStatuses::where('id', $lead->status_id)->first();
        Logs::set($lead->id, LOG_change_status, 'Изменён статус на "' . $statusText->name . '"');
    }

    private function getLastComment(int $lead_id)
    {
        $logs =  OrdersLog::where('lead_id', $lead_id)
            ->where('log_type_id', LOG_comment)
            ->orderBy('updated_at', 'DESC')
            ->first();

        if ($logs)
            return $logs;

        return null;
    }

}
