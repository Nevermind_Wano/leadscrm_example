<?php

namespace App\Http\Controllers\Orders;

use App\Models\LeadsStatuses;
use App\Models\OrdersLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Leads;
use App\Models\Orders;
use App\Helpers\Logs;
use App\Models\OrdersStatuses;

class OrdersController extends Controller
{
    public function index()
    {
        return view('orders.index');
    }

    public function filter (Request $request)
    {
        $query = Orders::query();

//        if (!$request->withOld)
//            $query->where('status_id', '<>', LEAD_STATUS_accepted);


        if($request->search) {
            $query->where('name', 'LIKE', '%'.$request->search.'%');
        }

        $orders = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

//        foreach ($orders as $order)
//            $order->status = $order->status()->first()->name;

        foreach ($orders as $order)
        {
            $order->status = OrdersStatuses::find($order->status_id);
            $lastComment = Orders::getLastComment($order->lead_id);
            $order->lastComment = ($lastComment) ? $lastComment->data : null;

            $order->goods = $order->goods()->get();

            foreach ($order->goods as $good)
            {
                $good->customFields = $good->customFields()->get();
            }
        }

        return $orders;
    }

    public function createByLead(Leads $lead)
    {
        $lead_id = $lead->id;
        return view('orders.create',compact(['lead_id']));
    }

    public function create()
    {
        return view('orders.create');
    }

    public function update(Request $request, int $id)
    {
        $this->store($request, $id);
    }

    public function store(Request $request, $update = 0)
    {
        $data = (object)$request->all();

        if ($update === 0)
            $order = new Orders();
        else
            $order = Orders::find($update);


        $order->fill($data->order);
        $order->lead_id = $data->lead_id;
        $order->deliveryType = $data->order['deliveryType'];
        $order->save();



        $goods = $data->orderGoods;

        foreach ($data->concomitants as $good)
        {
            if (isset($good['goods']) && !empty($good['goods']))
            {
                foreach ($good['goods'] as $_conGood)
                    array_push($goods, $_conGood);
                continue;
            }
            array_push($goods, $good);
        }



        $order->attachGoods($goods);

        $lead = Leads::where('id', $order->lead_id)->first();
        $lead->status_id = LEAD_STATUS_accepted;
        $lead->save();

        $lastComment = Orders::getLastComment($order->lead_id);

        if ($lastComment && !($lastComment->data === $data->comment))
            Logs::set($order->lead_id, LOG_comment, $data->comment);



        Logs::set($order->lead_id, LOG_change_status, 'Лид успешно закрыт');
        Logs::set($order->lead_id, LOG_change_type, 'Сформирован заказ id = ' . $order->id);
    }

    public function getListNewOrders()
    {
        $orders = Orders::where('status_id', ORDER_STATUS_new)->get();

        foreach ($orders as $order)
        {
            $order->goods = $order->goods()->get();

            foreach ($order->goods as $good)
            {
                $good->customFields = $good->customFields()->get();
            }
        }



        return $orders;

    }

    public function getOne(Orders $order)
    {
        return $order;
    }

    public function toPackage(Request $request)
    {
        $data = $request->all();

       foreach ($data as $value)
       {
           $order = Orders::find($value);
            $order->status_id = ORDER_STATUS_to_package;
            $order->save();

           $statusText = OrdersStatuses::where('id', $order->status_id)->first();
           Logs::set($order->lead_id, LOG_change_status, 'Изменён статус на "' . $statusText->name . '"');
       }
    }

    public function getAllStatus(Request $request)
    {
        return OrdersStatuses::all();
    }

    public function changeStatus(Request $request)
    {
        $data = (object)$request->all();

        $order = Orders::find($data->order_id);
        $order->status_id = $data->status;
        $order->save();


        $statusText = OrdersStatuses::where('id', $order->status_id)->first();
        Logs::set($order->lead_id, LOG_change_status, 'Изменён статус на "' . $statusText->name . '"');
    }

    public function delete(Orders $order)
    {
        $order->remove();
    }

    private function getLastComment(int $lead_id)
    {
        $logs =  OrdersLog::where('lead_id', $lead_id)
            ->where('log_type_id', LOG_comment)
            ->orderBy('updated_at', 'DESC')
            ->first();

        if ($logs)
            return $logs;

        return null;
    }

}
