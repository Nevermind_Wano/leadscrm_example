<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LeadSend extends Mailable
{
    use Queueable, SerializesModels;

    private $data;
    private $projectName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($projectName, array $data = [])
    {
        $this->data = $data;
        $this->projectName = $projectName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $projectName = $this->projectName;
        return $this->subject('New lead from project ' . $projectName)
            ->from('tovarka@pimac.ru')
            ->view('mail', compact(['projectName', 'data']));
    }
}
