<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FieldTypes extends Model
{
    protected $table = 'field_types';

    public $timestamps = false;

    protected $guarded = [];
}
