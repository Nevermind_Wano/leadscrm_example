<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model
{
    protected $table = 'goods';

    protected $guarded = [];

    public function projects()
    {
        return $this->belongsToMany('App\Models\Projects', 'goods_projects', 'good_id', 'project_id');
    }

    public function general()
    {
        return $this->belongsToMany('App\Models\Goods', 'concomitant_goods', 'concomitant_id', 'good_id');
    }

    public function concomitants()
    {
        return $this->belongsToMany('App\Models\Goods', 'concomitant_goods', 'good_id', 'concomitant_id');
    }

    public function customFields()
    {
        return $this->hasMany('App\Models\GoodsCF', 'good_id', 'id');
    }

    public function scopeVariantsCount($query)
    {
        return $query->select(['*', DB::raw('(SELECT count(*) FROM goods g WHERE g.is_variant = goods.id) as variants_count' )]);
    }

    public function scopeVariants($query, $id)
    {
        return $query->where('is_variant', $id);
    }

    public function scopeWithVariants($query)
    {
        $_id = $query->getBindings()[0];
        $conJoin = $query->getQuery()->joins[0];
        $conJoin->type = 'left';
        $query->getQuery()->joins[0] = $conJoin;
        $query->orWhere(function($query) use ($_id) {
              $query->whereRaw("is_variant IN (SELECT id FROM goods g
                                            LEFT JOIN concomitant_goods cg on g.`id` = cg.`concomitant_id`
                                            WHERE cg.`good_id` = {$_id})");
        });
        return $query;
    }

    public function attachProjects($projects)
    {
        $this->projects()->detach();
        foreach ($projects as $project)
        {
            $this->projects()->attach($project['id']);
        }
    }

    public function attachConcomitants($concomitants)
    {
        $this->concomitants()->detach();
        foreach ($concomitants as $concomitant)
        {
            $this->concomitants()->attach($concomitant['id']);
        }
    }

    public function attachGenerals($generals)
    {
        $this->general()->detach();
        foreach ($generals as $general)
        {
            $this->general()->attach($general['id']);
        }
    }

    public function attachCF($fields)
    {
        $this->customFields()->delete();

        foreach ($fields as $field)
        {
            unset($field['id']);
            if (isset($field['type']['type']))
               $field['type'] = $field['type']['type'];
            $this->customFields()->create($field);
        }
    }


}
