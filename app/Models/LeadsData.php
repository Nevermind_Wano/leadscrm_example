<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadsData extends Model
{
    protected $table = 'leads_data';

    protected $guarded = [];

    public static function getData($lead_id)
    {
        return LeadsData::select([
            'leads_data.id',
            'fields.field_name as field',
            'leads_data.data',
            'leads_data.lead_id'
        ])
            ->join('fields', 'leads_data.field_id', '=', 'fields.id')
            ->where('leads_data.lead_id', $lead_id)
            ->get();
    }
}
