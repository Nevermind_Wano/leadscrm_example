<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogTypes extends Model
{
    protected $table = 'log_types';

    public $timestamps = false;

    protected $guarded = [];
}
