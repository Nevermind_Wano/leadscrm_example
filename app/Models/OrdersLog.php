<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdersLog extends Model
{
    protected $table = 'orders_log';

    protected $guarded = [];

    public function type()
    {
        $this->hasOne('App\Models\LogTypes');
    }
}
