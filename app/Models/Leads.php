<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Projects;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class Leads extends Model
{

    use SoftDeletes;

    protected $table = 'leads';

    protected $guarded = [];

    public function data()
    {
        return $this->hasMany('App\Models\LeadsData', 'lead_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\LeadsStatuses');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Projects');
    }

    public function log()
    {
        return $this->hasOne('App\Models\OrdersLog', 'lead_id', 'id');
    }

    public static function countDoubles($lead_id)
    {
        $query = "SELECT                   
                    `field_id`,
                    `data`,
                    COUNT(*) AS `count`
                FROM
                    `leads_data`
                WHERE lead_id = {$lead_id}
                GROUP BY
                    `field_id`,`data`
                HAVING 
                    `count` > 1";

        return
            DB::select(DB::raw($query));

    }

    public function findDoubles()
    {

    }

    public function saveData($data)
    {
        $this->data()->delete();

        $mailData = [];

        foreach ($data as $key => $value)
        {
            $_leadData = [];
            $double = false;
            $doubleID = 0;
//            dump($key);
            $_leadData['field_id'] = Fields::where('project_id', $this->project_id)
                ->where('field_name', $key)
                ->first()
                ->id;

            if ($key === 'phone') {
                $phone = LeadsData::where('field_id', $_leadData['field_id'])
                            ->where('data', $value)
                            ->first();

//                dd($phone);
                if (!empty($phone))
                {
                    $_phone = substr($phone->data, -10);
                    $_value = substr($value, -10);

                    if($_phone === $_value)
                    {
                        $double = true;
                        $doubleID = $phone->lead_id;
                    }
                }
            }
            $mailData[$key] = $value;

            $_leadData['data'] = $value;

            $this->data()->create($_leadData);

        }
        return [$double, $doubleID, $mailData];

    }

    public function remove()
    {
//        $this->data()->delete();
//        $this->log()->delete();
        $this->delete();
    }
}
