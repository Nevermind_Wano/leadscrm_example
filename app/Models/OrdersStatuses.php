<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdersStatuses extends Model
{
    protected $table = 'orders_statuses';

    public $timestamps = false;

    protected $guarded = [];
}
