<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    protected $guarded = [];

    public function goods()
    {
        return $this->belongsToMany('App\Models\Goods', 'orders_goods', 'order_id', 'good_id')->withPivot('count', 'client_price');
    }

    public function lead()
    {
        return $this->belongsTo('App\Models\Leads');
    }

    public static function getLastComment(int $lead_id)
    {
        $logs =  OrdersLog::where('lead_id', $lead_id)
            ->where('log_type_id', LOG_comment)
            ->orderBy('updated_at', 'DESC')
            ->first();

        if ($logs)
            return $logs;

        return null;
    }

    public function attachGoods($goods)
    {
        $this->goods()->detach();
        foreach ($goods as $good)
        {
            if (!empty($good['clientCount']) && $good['clientCount']!== 0)
            {
                $good['clientPrice'] = intval($good['clientPrice']);

                $this->goods()->attach($good['id'], [
                    'count'        => $good['clientCount'],
                    'client_price' => ($good['clientPrice'] !== 0) ? $good['clientPrice'] : $good['price']
                ]);
                $_good = Goods::find($good['id'])->first();
                $_good->count = $_good->count - $good['clientCount'];
                $_good->save();
            }
        }
    }

    public function remove()
    {
        $this->goods()->detach();

        foreach ($this->goods as $good)
        {
            $_good = Goods::find($good['id'])->first();
            $_good->count = $_good->count + $good['clientCount'];
            $_good->save();
        }
        $lead_id = $this->lead_id;
        $this->delete();

        $lead = Leads::find($lead_id);
        $lead->remove();

    }
}
