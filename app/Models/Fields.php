<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fields extends Model
{
    protected $table = 'fields';

    protected $guarded = [];

    public function types()
    {
        return $this->hasOne('App\FieldTypes');
    }
}
