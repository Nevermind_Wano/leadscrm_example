<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Projects extends Model
{
    use SoftDeletes;

    protected $table = 'projects';

    protected $guarded = [];

    public function goods()
    {
        return $this->belongsToMany('App\Models\Goods', 'goods_projects', 'project_id', 'good_id');
    }

    public function fields()
    {
        return $this->hasMany('App\Models\Fields', 'project_id', 'id');
    }

    public function attachFields($fields)
    {
        $this->fields()->delete();

        foreach ($fields as $field)
        {
            unset($field['id']);
            $field['field_type'] = $field['field_type']['id'];
            $this->fields()->create($field);
        }

    }
    public function attachGoods($goods)
    {
        if (empty($goods))
            return;

        $this->goods()->detach();
        $this->goods()->attach($goods['id']);
    }
}
