<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadsStatuses extends Model
{
    protected $table = 'leads_statuses';

    public $timestamps = false;

    protected $guarded = [];
}
