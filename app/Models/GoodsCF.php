<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsCF extends Model
{
    protected $table = 'goods_custom_fields';

    protected $guarded = [];

    public $timestamps = false;


}
