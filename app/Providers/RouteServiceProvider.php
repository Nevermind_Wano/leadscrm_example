<?php

namespace App\Providers;

use App\Models\LeadsData;
use App\Models\Leads;
use App\Models\Orders;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\Models\Goods;
use App\Models\Projects;
use App\Models\OrdersLog;
use Illuminate\Support\Facades\DB;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('order_log', function ($lead_id) {
            return OrdersLog::select([
                'orders_log.*'
            ])
                ->where('lead_id', $lead_id)
                ->orderBy('created_at', 'DESC')
                ->get();
        });

        Route::bind('good', function ($good_id) {
            $goods = Goods::select([
                'goods.*'
            ])
                ->where('goods.id', $good_id)
                ->orWhere('goods.is_variant', $good_id)
                ->get();

            foreach ($goods as $good){
                $good->customFields = $good->customFields()->get();
            }

            return $goods;
        });

        Route::bind('good_variants', function ($good_id) {
            $goods = Goods::where('id', $good_id)->get();
            $merged = $goods->merge(Goods::variants($good_id)->get());
            foreach ($merged->all() as $good)
                $good->customFields = $good->customFields()->get();

            return $merged;
        });

        Route::bind('goods_lead', function ($lead_id) {
            $project = Leads::find($lead_id)->project()->first();
            $goods = $project->goods()->get();
            if (!empty($goods))
                $merged = $goods->merge(Goods::variants($goods[0]->id)->get());
            else
                $merged = $goods;
            foreach ($merged as $good)
            {
                $good->customFields = $good->customFields()->get();
                $good->clientCount = 0;
                $good->clientPrice = 0;
            }
            return $merged;
        });

        Route::bind('projects', function ($project_id) {
            return Projects::select([
                'projects.*'
            ])
                ->where('projects.id', $project_id)
                ->firstOrFail();
        });

        Route::bind('lead', function ($lead_id) {
            return Leads::select([
                'leads.*'
            ])
                ->where('leads.id', $lead_id)
                ->firstOrFail();
        });

        Route::bind('lead_data', function ($lead_id) {
            return LeadsData::getData($lead_id);
        });

        Route::bind('order', function ($order_id) {
           $order = Orders::find($order_id);


           $order->goods = $order->goods()->get();

            $lastComment = Orders::getLastComment($order->lead_id);
            $order->lastComment = ($lastComment) ? $lastComment->data : null;

           foreach ($order->goods as $good)
           {
               $good->customFields = $good->customFields()->get();
               $good->clientPrice = $good->pivot->client_price;
               $good->clientCount = $good->pivot->count;

           }


           return $order;
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
