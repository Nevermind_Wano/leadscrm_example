<?php

use Illuminate\Database\Seeder;
use App\Models\OrdersStatuses;

require(base_path() . '/config/constants.php');

class OrdersStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\OrdersStatuses::create([
            'id' => ORDER_STATUS_new,
            'slug' => 'new',
            'name' => 'Новый',
            'description' => 'Лид обработан и сформирован заказ'
        ]);

        \App\Models\OrdersStatuses::create([
            'id' => ORDER_STATUS_to_package,
            'slug' => 'to_package',
            'name' => 'Отправлен на комплектацию',
            'description' => ''
        ]);

        \App\Models\OrdersStatuses::create([
            'id' => ORDER_STATUS_to_transport,
            'slug' => 'to_transport',
            'name' => 'Отправлен в транспортную компанию',
            'description' => ''
        ]);
    }
}
