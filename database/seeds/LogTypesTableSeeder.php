<?php

use Illuminate\Database\Seeder;


class LogTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\LogTypes::create([
            'id'    => LOG_change_status,
            'name' => 'change_status',
        ]);

        \App\Models\LogTypes::create([
            'id'    => LOG_change_type,
            'name' => 'change_type',
        ]);

        \App\Models\LogTypes::create([
            'id'    => LOG_comment,
            'name' => 'comment',
        ]);

        \App\Models\LogTypes::create([
            'id'    => LOG_error,
            'name' => 'error',
        ]);
    }
}
