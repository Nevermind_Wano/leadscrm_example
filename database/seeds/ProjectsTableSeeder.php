<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'projects',
            'display_name' => 'Проекты',
            'icon' => 'icon-briefcase'
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-projects',
                'display_name' => 'Просмотр',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'add-projects',
                'display_name' => 'Добавить',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'edit-projects',
                'display_name' => 'Редактировать',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-projects',
                'display_name' => 'Удаление',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
        ]);

        // Assign permissions to admin role
        $admin = Role::findByName('admin');
        $admin->givePermissionTo(Permission::all());

        // Assign permissions to user role
        $user = Role::findByName('user');
        $user->givePermissionTo('read-projects');
    }
}
