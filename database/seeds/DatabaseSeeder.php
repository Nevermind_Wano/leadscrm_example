<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(DashboardTableSeeder::class);
        $this->call(GoodsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(FieldsTypesTableSeeder::class);
        $this->call(LeadsStatusesTableSeeder::class);
        $this->call(OrdersStatusesTableSeeder::class);
        $this->call(LogTypesTableSeeder::class);
    }
}
