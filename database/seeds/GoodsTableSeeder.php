<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class GoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'goods',
            'display_name' => 'Товары',
            'icon' => 'icon-briefcase'
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-goods',
                'display_name' => 'Просмотр',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'add-goods',
                'display_name' => 'Добавить',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'edit-goods',
                'display_name' => 'Редактировать',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-goods',
                'display_name' => 'Удаление',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
        ]);

        // Assign permissions to admin role
        $admin = Role::findByName('admin');
        $admin->givePermissionTo(Permission::all());

        // Assign permissions to user role
        $user = Role::findByName('user');
        $user->givePermissionTo('read-goods');
    }
}
