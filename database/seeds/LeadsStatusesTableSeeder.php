<?php

use Illuminate\Database\Seeder;
use App\Models\LeadsStatuses;

require(base_path() . '/config/constants.php');

class LeadsStatusesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\LeadsStatuses::create([
            'id' => LEAD_STATUS_new,
            'slug' => 'new',
            'name' => 'Новый',
            'description' => 'Ещё не принят в обработку'
        ]);

        \App\Models\LeadsStatuses::create([
            'id' => LEAD_STATUS_accepted,
            'slug' => 'accepted',
            'name' => 'Принят в обработку',
            'description' => ''
        ]);

        \App\Models\LeadsStatuses::create([
            'id' => LEAD_STATUS_callback,
            'slug' => 'callback',
            'name' => 'Перезвонить',
            'description' => ''
        ]);

        \App\Models\LeadsStatuses::create([
            'id' => LEAD_STATUS_reject,
            'slug' => 'reject',
            'name' => 'Отказ',
            'description' => ''
        ]);

        \App\Models\LeadsStatuses::create([
            'id' => LEAD_STATUS_no_call_3_day,
            'slug' => 'no_call_3_day',
            'name' => 'Недозвон 3 дня',
            'description' => ''
        ]);

        \App\Models\LeadsStatuses::create([
            'id' => LEAD_STATUS_double,
            'slug' => 'double',
            'name' => 'Дубль',
            'description' => ''
        ]);
    }
}
