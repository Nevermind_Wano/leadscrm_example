<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('is_variant')->unsigned()->nullable();
            $table->integer('in_kit')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('purchase_price')->unsigned();
            $table->integer('price')->unsigned();
            $table->integer('weight')->unsigned();
            $table->string('image_url')->nullable();
            $table->integer('count')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('in_kit')->references('id')->on('goods_kits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
    }
}
