<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id')->unsigned();
            $table->integer('status_id')->default(0);
            $table->string('client_name');
            $table->string('telephone');
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('zip')->nullable();
            $table->string('point_code')->nullable();
            $table->string('priceDelivery');
            $table->string('priceDeliveryClient');
            $table->string('deliveryType')->default('boxberry');
            $table->longText('comment')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('lead_id')->references('id')->on('leads');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
