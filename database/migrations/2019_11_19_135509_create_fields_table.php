<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->string('field_name');
            $table->integer('field_type')->unsigned();
            $table->string('wrapper_class')->default('form-group')->nullable();
            $table->string('attr_class')->default('form-control')->nullable();
            $table->string('label')->default('default')->nullable();
            $table->string('label_class')->default('control-label')->nullable();
            $table->string('errors_class')->default('text-danger')->nullable();
            $table->string('error_messages')->default('')->nullable();
            $table->boolean('required_field')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('field_type')->references('id')->on('field_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
